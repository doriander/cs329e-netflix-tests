#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, calculate_rmse, calculate_customer_rating_deviation, create_cache
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------
AVERAGE_CUSTOMER_RATING = create_cache("cache-averageCustomerRating.pickle")

class TestNetflix (TestCase):

    # ----
    # eval tests
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n2.4\n2.4\n2.4\n0.90\n")
    


    # ----
    # unit tests for rmse values
    # ----

    def test_rmse_value(self):
        predictions = [2.4, 2.4, 2.1, 0.9]
        actual =  [3.1, 2.0, 1.9, 2.1]
        rmse = calculate_rmse(predictions, actual) 
        self.assertEqual(rmse, 0.7297259759663214)
    
    def test_rmse_value_with_negative_values(self):
        predictions = [1, 2, 3, 4]
        actual = [4, -1, -3, 2]
        rmse = calculate_rmse(predictions, actual) 
        self.assertEqual(rmse, 3.8078865529319543)
        
    def test_rmse_value_less_than_acceptable_range(self):
        predictions = [1, 2, 3, 4]
        actual = [1, 3, 4, 5]
        rmse = calculate_rmse(predictions, actual) 
        self.assertLess(rmse , 1.00)

    # ----
    # unit tests for calculate_customer_rating_deviation
    #   info: deviation of average rating of all movies and average rantings of each customer
    # ----


    def test_calculate_customer_rating_deviation_value(self):
        rating_sum = 0
        for key in AVERAGE_CUSTOMER_RATING.keys():
            rating_sum = rating_sum + AVERAGE_CUSTOMER_RATING[key]
        average_rating_all_customers = rating_sum/len(AVERAGE_CUSTOMER_RATING)
        deviation = calculate_customer_rating_deviation(int(2097140), average_rating_all_customers)
        self.assertEqual(deviation, 0.025894191661789634)

    def test_calculate_customer_rating_deviation_no_such_customer(self):
        rating_sum = 0
        for key in AVERAGE_CUSTOMER_RATING.keys():
            rating_sum = rating_sum + AVERAGE_CUSTOMER_RATING[key]
        average_rating_all_customers = rating_sum/len(AVERAGE_CUSTOMER_RATING)
        deviation = calculate_customer_rating_deviation(int(99999999999), average_rating_all_customers)
        self.assertEqual(deviation, 0)




# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""